## Настройка данных для тестового стенда

1. В файле ..\src\main\java\model\EndPoints.java в переменной mainUrl указывается адресс стенда
2. В файле ..\src\main\java\util\DBClient.java указываются данные для подключения к БД стенда 
3. В файле ..\src\main\java\model\LeadRequests.java указываются Api Key для пользователей
4. В файле ..\src\main\java\model\LeadResponses.java указывается версия

