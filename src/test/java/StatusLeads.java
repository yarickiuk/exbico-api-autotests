import model.*;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.equalTo;

public class StatusLeads {

    @Test
    public void getForbiddenStatusLeads() {
        given()
                .relaxedHTTPSValidation()
                .spec(LeadRequests.WRONG_TOKEN_REQUEST)
                .body(LeadRequests.idRejectedLead)
                .when()
                .post(EndPoints.statusLeads)
                .then()
                .spec(LeadResponses.WRONG_TOKEN_RESPONSE);
    }

    @Test
    public void getInProgressStatusLeads() {
        given()
                .relaxedHTTPSValidation()
                .spec(LeadRequests.TEST_USER_TOKEN_REQUEST)
                .body(LeadRequests.idInProgressLead)
                .when()
                .post(EndPoints.statusLeads)
                .then()
                .spec(LeadResponses.INPROGRESS_LEAD_STATUS_RESPONSE);
    }

    @Test
    public void getStatusLeadsEmptyResponse() {
        given()
                .relaxedHTTPSValidation()
                .spec(LeadRequests.TEST_USER_TOKEN_REQUEST)
                .when()
                .post(EndPoints.statusLeads)
                .then()
                .spec(LeadResponses.SUCCESS_RESPONSE_FOR_EMPTY_REQUEST)
                .and()
                .body("data", equalTo(null));
    }

    @Test
    public void getAuthErrorStatus() {
        given()
                .relaxedHTTPSValidation()
                .spec(LeadRequests.WO_TOKEN_REQEUST)
                .when()
                .post(EndPoints.statusLeads)
                .then()
                .spec(LeadResponses.WRONG_TOKEN_RESPONSE)
                .and()
                .body("message", equalTo("Wrong token"));
    }

    @Test
    public void getNotFoundStatus() {
        given()
                .relaxedHTTPSValidation()
                .spec(LeadRequests.TEST_USER_TOKEN_REQUEST)
                .body(LeadRequests.wrongLeadsNumber)
                .when()
                .post(EndPoints.statusLeads)
                .then()
                .spec(LeadResponses.NOT_FOUND_STATUS_RESPONSE)
                .and()
                .body("data", equalTo(LeadResponses.expectedJsonToNotFoundStatus.getList("")));
    }
}