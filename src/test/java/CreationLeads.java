import model.*;
import helper.*;
import org.junit.Test;

import java.sql.SQLException;

import static io.restassured.RestAssured.given;

public class CreationLeads {
    @Test
    public void getSuccessStatusCreationLead() throws SQLException {
        SqlScriptLeadHelper.updatePhoneNumberOfLeadsCreatedByAutotest();
        given()
                .relaxedHTTPSValidation()
                .spec(LeadRequests.TEST_USER_TOKEN_REQUEST)
                .body(LeadsData.getNewLeadData().toString())
                .when()
                .post(EndPoints.createLeads)
                .then()
                .spec(LeadResponses.SUCCESS_RESPONSE_FOR_CREATE_LEADER);
    }

    @Test
    public void getFailedStatusForDouble() throws SQLException {
        given()
                .relaxedHTTPSValidation()
                .spec(LeadRequests.TEST_USER_TOKEN_REQUEST)
                .body(LeadsData.getNewLeadData().toString())
                .when()
                .post(EndPoints.createLeads)
                .then()
                .spec(LeadResponses.SUCCESS_RESPONSE_FOR_DOUBLE);
    }

    @Test
    public void getFailedStatusForIncorrectData() {
        given()
                .relaxedHTTPSValidation()
                .spec(LeadRequests.TEST_USER_TOKEN_REQUEST)
                .body(LeadsData.getIncorrectLeadData().toString())
                .when()
                .post(EndPoints.createLeads)
                .then()
                .spec(LeadResponses.SUCCESS_RESPONSE_FOR_INCORRECT_DATA);
    }
}