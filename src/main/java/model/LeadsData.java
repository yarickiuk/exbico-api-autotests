package model;

import org.json.JSONObject;

public class LeadsData {

    public static JSONObject getNewLeadData() {
        JSONObject leadObject = new JSONObject();
        JSONObject providerObject = new JSONObject();
        JSONObject productObject = new JSONObject();

        JSONObject nameLocationObject = new JSONObject();
        JSONObject exbicoLocationObject = new JSONObject();
        JSONObject kladrLocationObject = new JSONObject();
        JSONObject locationObject = new JSONObject();

        JSONObject employmentClientObject = new JSONObject();
        JSONObject metaObject = new JSONObject();
        JSONObject clientObject = new JSONObject();

        productObject.put("typeId", "consumer");
        productObject.put("amount", 10000000);
        productObject.put("term", "5");
        productObject.put("initialFee", "0");
        productObject.put("bidId", "123456798");

        nameLocationObject.put("region", "");
        nameLocationObject.put("city", "");
        exbicoLocationObject.put("regionId", 55);
        exbicoLocationObject.put("cityId", 62);
        kladrLocationObject.put("cityId", "");

        providerObject.put("apiKey", LeadRequests.providerApiKey);
        locationObject.put("name", nameLocationObject);
        locationObject.put("exbico", exbicoLocationObject);
        locationObject.put("kladr", kladrLocationObject);

        employmentClientObject.put("income", "50000");
        employmentClientObject.put("hasIncomeCert2NDFL", "1");
        employmentClientObject.put("hasIncomeCertByBankForm", "1");
        clientObject.put("firstName", "firstName_autotest");
        clientObject.put("patronymic", "patronymic_autotest");
        clientObject.put("lastName", "lastName_autotest");
        clientObject.put("age", "46");
        clientObject.put("birthDate", "1992-11-01");
        clientObject.put("phone", "+7(999)123-45-67");
        clientObject.put("email", "palchikov@exbico.ru");
        clientObject.put("employment", employmentClientObject);

        metaObject.put("utmSource", "test");
        metaObject.put("utmMedium", "test");
        metaObject.put("utmCampaign ", "test");
        metaObject.put("utmTerm", "test");
        metaObject.put("utmContent", "test");

        leadObject.put("provider", providerObject);
        leadObject.put("product", productObject);
        leadObject.put("location", locationObject);
        leadObject.put("client", clientObject);
        leadObject.put("meta", metaObject);
        leadObject.put("meta", metaObject);
        leadObject.put("agreedWithPersonalDataTransfer", true);

        return leadObject;
    }

    public static JSONObject getIncorrectLeadData() {
        JSONObject leadObject = new JSONObject();
        JSONObject providerObject = new JSONObject();
        JSONObject productObject = new JSONObject();

        JSONObject nameLocationObject = new JSONObject();
        JSONObject exbicoLocationObject = new JSONObject();
        JSONObject kladrLocationObject = new JSONObject();
        JSONObject locationObject = new JSONObject();

        JSONObject employmentClientObject = new JSONObject();
        JSONObject metaObject = new JSONObject();
        JSONObject clientObject = new JSONObject();

        productObject.put("typeId", "consumer");
        productObject.put("amount", 10000000);
        productObject.put("term", "5 лет");
        productObject.put("initialFee", "0");
        productObject.put("bidId", "123456798");

        nameLocationObject.put("region", "");
        nameLocationObject.put("city", "");
        exbicoLocationObject.put("regionId", 66);
        exbicoLocationObject.put("cityId", 74);
        kladrLocationObject.put("cityId", "");

        providerObject.put("apiKey", LeadRequests.providerApiKey);
        locationObject.put("name", nameLocationObject);
        locationObject.put("exbico", exbicoLocationObject);
        locationObject.put("kladr", kladrLocationObject);

        employmentClientObject.put("income", "50000");
        employmentClientObject.put("hasIncomeCert2NDFL", "1");
        employmentClientObject.put("hasIncomeCertByBankForm", "1");
        clientObject.put("firstName", "firstName_autotest_double");
        clientObject.put("patronymic", "patronymic_autotest_double");
        clientObject.put("lastName", "lastName_autotest_double");
        clientObject.put("age", "46");
        clientObject.put("birthDate", "1001-11-01");
        clientObject.put("phone", "+7(999)123-45-67");
        clientObject.put("email", "palchikov@exbico.ru");
        clientObject.put("employment", employmentClientObject);

        metaObject.put("utmSource", "test");
        metaObject.put("utmMedium", "test");
        metaObject.put("utmCampaign ", "test");
        metaObject.put("utmTerm", "test");
        metaObject.put("utmContent", "test");

        leadObject.put("provider", providerObject);
        leadObject.put("product", productObject);
        leadObject.put("location", locationObject);
        leadObject.put("client", clientObject);
        leadObject.put("meta", metaObject);
        leadObject.put("meta", metaObject);
        leadObject.put("agreedWithPersonalDataTransfer", true);

        return leadObject;
    }
}
