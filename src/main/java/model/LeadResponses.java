package model;

import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.ResponseSpecification;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;

public class LeadResponses {

    private static String version = "3.0.3";
    private static String successStatus = "success";
    private static String failStatus = "fail";
    private static String messageSuccessStatus = null;
    private static String codeSuccessStatus = null;

    public static JsonPath expectedJsonToNotFoundStatus = new JsonPath("[{\"leadStatus\":\"notFound\", \"leadId\":9000000}]");
    public static JsonPath expectedJsonToInProgressStatus = new JsonPath("[{\"leadStatus\":\"inProgress\", \"leadId\":" + LeadRequests.inProgressStatusLeadId + "}]");
    public static JsonPath expectedJsonToDoubleResponse = new JsonPath("{\"leadStatus\":\"rejected\", \"rejectReason\":\"isDouble\"}");
    public static JsonPath expectedJsonToIncorrectResponse = new JsonPath("[\"Birthday should be in format: yyyy-mm-dd (from 0 to 100 years)\"]");
    public static String expectedMessageToIncorrectResponse = "Can not save LidLeads: {\"birthday\":[\"\\u041d\\u0435\\u043a\\u043e\\u0440\\u0440\\u0435\\u043a\\u0442\\u043d\\u0430\\u044f \\u0434\\u0430\\u0442\\u0430 \\u0440\\u043e\\u0436\\u0434\\u0435\\u043d\\u0438\\u044f\"]}";

    public static ResponseSpecification SUCCESS_RESPONSE_FOR_EMPTY_REQUEST;
    public static ResponseSpecification WRONG_TOKEN_RESPONSE;
    public static ResponseSpecification INPROGRESS_LEAD_STATUS_RESPONSE;
    public static ResponseSpecification NOT_FOUND_STATUS_RESPONSE;
    public static ResponseSpecification SUCCESS_RESPONSE_FOR_CREATE_LEADER;
    public static ResponseSpecification SUCCESS_RESPONSE_FOR_DOUBLE;
    public static ResponseSpecification SUCCESS_RESPONSE_FOR_INCORRECT_DATA;
    public static ResponseSpecification SUCCESS_RESPONSE_FOR_RETURN_LEADER;

    static {
        SUCCESS_RESPONSE_FOR_EMPTY_REQUEST = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectBody("status", equalTo(successStatus))
                .expectBody("message", equalTo(messageSuccessStatus))
                .expectBody("version", equalTo(version))
                .expectBody("code", equalTo(codeSuccessStatus))
                .expectBody("data", equalTo(null))
                .build();
        WRONG_TOKEN_RESPONSE = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectBody("status", equalTo(failStatus))
                .expectBody("version", equalTo(version))
                .expectBody("message", equalTo("Wrong token"))
                .expectBody("data", equalTo(null))
                .expectBody("code", equalTo(codeSuccessStatus))
                .build();

        INPROGRESS_LEAD_STATUS_RESPONSE = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectBody("status", equalTo(successStatus))
                .expectBody("message", equalTo(messageSuccessStatus))
                .expectBody("version", equalTo(version))
                .expectBody("code", equalTo(codeSuccessStatus))
                .expectBody("data", equalTo(expectedJsonToInProgressStatus.getList("")))
                .build();
        NOT_FOUND_STATUS_RESPONSE = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectBody("status", equalTo(successStatus))
                .expectBody("message", equalTo(messageSuccessStatus))
                .expectBody("version", equalTo(version))
                .expectBody("code", equalTo(codeSuccessStatus))
                .expectBody("data", equalTo(expectedJsonToNotFoundStatus.getList("")))
                .build();

        SUCCESS_RESPONSE_FOR_CREATE_LEADER = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectBody("status", equalTo(successStatus))
                .expectBody("message", equalTo(messageSuccessStatus))
                .expectBody("version", equalTo(version))
                .expectBody("code", equalTo(codeSuccessStatus))
                .expectBody("data", notNullValue())
                .build();

        SUCCESS_RESPONSE_FOR_DOUBLE = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectBody("status", equalTo(successStatus))
                .expectBody("message", equalTo(messageSuccessStatus))
                .expectBody("version", equalTo(version))
                .expectBody("code", equalTo(codeSuccessStatus))
                .expectBody("data", equalTo(expectedJsonToDoubleResponse.get()))
                .build();
        SUCCESS_RESPONSE_FOR_INCORRECT_DATA = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectBody("status", equalTo(failStatus))
                .expectBody("message", equalTo(expectedMessageToIncorrectResponse))
                .expectBody("version", equalTo(version))
                .expectBody("code", equalTo(codeSuccessStatus))
                .expectBody("data", equalTo(expectedJsonToIncorrectResponse.getList("")))
                .build();
        SUCCESS_RESPONSE_FOR_RETURN_LEADER = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .build();
    }
}
