package model;

public final class EndPoints {
    public static final String mainUrl = "https://test2.app.exbico.ru";
    public static final String apiUrl = mainUrl + "/api/v3";
    public static final String apiReturnLeadUrl = mainUrl + "/api/leads/broker/v1";
    public static final String statusLeads = "/leads-status";
    public static final String createLeads = "/lead";
}
