package model;

import helper.SqlScriptLeadHelper;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;

import static io.restassured.RestAssured.oauth2;

public class LeadRequests {

    public static RequestSpecification WRONG_TOKEN_REQUEST;
    public static RequestSpecification WO_TOKEN_REQEUST;
    public static RequestSpecification TEST_USER_TOKEN_REQUEST;
    public static RequestSpecification TEST_USER_CONTRACT_TOKEN_REQUEST;

    public static String providerApiKey = "4e64de1424cea03956a180bc4e592eeb";
    public static String brokerApiKey = "7322c71e66f72ebb1cf52d9a6abc90ca";
    public static String wrongApiKey = "111eaf897c638d519710b16911211111";

    public static int inProgressStatusLeadId = SqlScriptLeadHelper.findIdInProgressLeadByApiKey(providerApiKey);
    public static String wrongLeadsNumber = "[9000000]";

    public static String idRejectedLead = "[" + SqlScriptLeadHelper.findIdRejectedLeadByApiKey(providerApiKey) + "]";
    public static String idInProgressLead = "[" + inProgressStatusLeadId + "]";

    static {
        WRONG_TOKEN_REQUEST = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri(EndPoints.apiUrl)
                .setAuth(oauth2(wrongApiKey))
                .build();
        TEST_USER_TOKEN_REQUEST = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri(EndPoints.apiUrl)
                .setAuth(oauth2(providerApiKey))
                .build();
        WO_TOKEN_REQEUST = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri(EndPoints.apiUrl)
                .build();
        TEST_USER_CONTRACT_TOKEN_REQUEST = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri(EndPoints.apiReturnLeadUrl)
                .setAuth(oauth2(brokerApiKey))
                .build();
    }

    public static JSONObject getJsonBodyForReturnLead() {
        JSONObject bodyObject = new JSONObject();
        bodyObject.put("reasonId", 2);
        bodyObject.put("comment", "comment by autotest");

        return bodyObject;
    }
}
