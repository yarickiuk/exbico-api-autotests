package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBClient {
    private static final String url = "jdbc:mysql://localhost:3308/exbico_devtest2";
    private static final String user = "novikov";
    private static final String password = "ShELadeFOReflENH";
    private static Connection connection;

    private static DBClient instance;

    private DBClient() throws SQLException {
        connection = DriverManager.getConnection(url, user, password);
    }

    public static DBClient getInstance() throws SQLException {
        if (instance == null) {
            instance = new DBClient();
        }
        return instance;
    }

    public String getUrlConnect() {
        return url;
    }

    public String getPassword() {
        return password;
    }

    public String getUser() {
        return user;
    }

    public Connection getConnection() {
        return connection;
    }
}
