package helper;

import com.mysql.jdbc.Connection;
import util.DBClient;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlScriptLeadHelper {

    private static Connection connection;

    static {
        try {
            connection = (Connection) DBClient.getInstance().getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Integer findIdCreatedByAutoTestLead() throws SQLException {
        Integer idLead;
        ResultSet info = connection.createStatement().executeQuery("" +
                "SELECT id " +
                "FROM lid_leads " +
                "WHERE lid_leads.phone = '+79991234567' " +
                "AND lid_leads.isDouble = 0");
        if (info.next()) {
            idLead = info.getInt("id");
            return idLead;
        }
        return null;
    }

    public static Integer findIdInProgressLeadByApiKey(String providerApiKey) {
        Integer idLead;
        ResultSet info = null;
        try {
            info = connection.createStatement().executeQuery("SELECT lead_id " +
                    "FROM lead_status " +
                    "INNER JOIN lid_leads as LL " +
                    "ON LL.id = lead_id " +
                    "INNER JOIN lead_suppliers as LS " +
                    "ON LS.id = LL.id_supplier " +
                    "WHERE LS.api_key = '" + providerApiKey + "' " +
                    "AND status_id = '2' " +
                    "LIMIT 1;");
            if (info.next()) {
                idLead = info.getInt("lead_id");
                return idLead;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Integer findIdRejectedLeadByApiKey(String providerApiKey) {
        Integer idLead;
        ResultSet info = null;
        try {
            info = connection.createStatement().executeQuery("SELECT lead_id " +
                    "FROM lead_status " +
                    "INNER JOIN lid_leads as LL " +
                    "ON LL.id = lead_id " +
                    "INNER JOIN lead_suppliers as LS " +
                    "ON LS.id = LL.id_supplier " +
                    "WHERE LS.api_key = '" + providerApiKey + "' " +
                    "AND status_id = '7' " +
                    "LIMIT 1;");
            if (info.next()) {
                idLead = info.getInt("lead_id");
                return idLead;
            }

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return null;
    }

    public static Integer getStatusLeadId(Integer idLead) throws SQLException {
        Integer lead_status;
        ResultSet info = connection.createStatement().executeQuery("" +
                "SELECT status_id " +
                "FROM lead_status " +
                "WHERE lead_status.lead_id = " + idLead);
        if (info.next()) {
            lead_status = info.getInt("status_id");
            return lead_status;
        }
        return null;
    }

    public static void updatePhoneNumberOfLeadsCreatedByAutotest() throws SQLException {
        connection.createStatement().executeUpdate("UPDATE lid_leads " +
                "SET lid_leads.phone = '+79991111222' " +
                "WHERE lid_leads.phone = '+79991234567'");
    }

    public static void deleteCreatedByAutoTestLead() throws SQLException {
        Integer id = SqlScriptLeadHelper.findIdCreatedByAutoTestLead();
        if (id != null) {
            connection.createStatement().executeUpdate("" +
                    "DELETE " +
                    "FROM lead_source " +
                    "WHERE lead_source.lead_id = '" + id + "'");

            connection.createStatement().executeUpdate("" +
                    "DELETE " +
                    "FROM lead_to_person " +
                    "WHERE lead_to_person.lead_id = '" + id + "'");

            connection.createStatement().executeUpdate("" +
                    "DELETE " +
                    "FROM lead_status " +
                    "WHERE lead_status.lead_id = '" + id + "'");

            connection.createStatement().executeUpdate("" +
                    "DELETE " +
                    "FROM lid_leads_to_condition " +
                    "WHERE lid_leads_to_condition.id_lead = '" + id + "'");

            connection.createStatement().executeUpdate("" +
                    "DELETE " +
                    "FROM finmedia_api_log " +
                    "WHERE finmedia_api_log.id_lead = '" + id + "'");

            connection.createStatement().executeUpdate("" +
                    "DELETE " +
                    "FROM lid_leads " +
                    "WHERE lid_leads.id = '" + id + "'");
        }
    }
}
