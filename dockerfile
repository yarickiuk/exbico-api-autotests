FROM gradle:4.7.0-jdk8-alpine AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon
CMD gradle test
#CMD ssh -N 3306:localhost:3308 test2.app.exbico.ru && ssh -N 3306:localhost:3308 172.17.0.1 && gradle test